
Positives:
1. Easy to parse - no API key required, no daility hit limit
2. JSON based - so quite standard and not a propreitary format

Negatives:
1. Man of the match/Player of the Match info missing
2. Player names are not standard - some players have full name (eg Ishan Kishan), some players have just surname (eg Pollard) while some players hace just first name (eg Rohit)

Concerns:
1. Sounds official but its not an official API - so no guarantee if it will be maintained on a long term basis - can we really build an application cround it ?